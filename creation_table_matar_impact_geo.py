from cassandra.cluster import Cluster

cluster = Cluster(['localhost'])
session = cluster.connect('projet_liujijie')

def creation_matar_impact_geo(session):

    query = '''CREATE TABLE matar_impact_geo (
                            date_year varint,
                            date_month varint,
                            date_day varint,
                            
                            date_hour varint,
                            date_min varint,
                            latitude float,
                            longitude float,
                            station text,
                            
                            tmpf float,
                            dwpf float,
                            relh float,
                            drct float,
                            sknt float,
                            alti float,
                            vsby float,
                            skyc1 text,
                            skyc2 text,
                            skyc3 text,
                            skyc4 text,
                            skyl1 float,
                            skyl2 float,
                            skyl3 float,
                            skyl4 float,
                            metar text,

                            PRIMARY KEY ((latitude, longitude), date_year, date_month, date_day, date_hour, date_min)
                            );'''
    session.execute(query)

