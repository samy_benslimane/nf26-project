# NF26-project

projet sur la seconde partie de l'UV NF26, crée par BENSLIMANE Samy et Jijie LIU.


## Nos Systèmes de stockage

### Impact géographique

La table '*matar_impact_geo*' a pour but de mettre en avant d’abord l’aspect géographique des données.


___Clé de partitionnement :___ latitude, longitude

___Clé de tri :___ Année, mois, jour, heure, minute

Les requêtes que nous effectuerons utilisant une dimension Géographique sont précises : on demande des localisation précise,
et nous des régions de données. Ainsi, il est plus pertinant de régler le partitionnement en fonction toute la latitude et toute la longitude.
A noter qu’après une étude des données, on a remarquer que les stations ne bougeais pas (ou presque pas).
Ainsi il n’était pas pertinent d’ajouter le nom de la station dans notre clé.

### Impact temporelle

La table '*matar_impact_temporelle*' a pour but de mettre en avant d’abord l’aspect temporelle des données.

___Clé de partitionnement :___ Année, mois, jour 

___Clé de tri :___ heure, minute, latitude, longitude

Les requêtes que nous effectuerons utilisant une dimension temporelle sont souvent sur la période jour-mois-année.

En ce qui concerne les indicateurs, tous les indicateurs ont été retenues (15 au total) pour les deux systèmes, 
pour une question d’adaptabilité dans le futur.


**Fichier de créations des tables :** 
-	creation_tables_matar_impact_geo.py
-	creation_tables_matar_impact_temporelle.py



## Ecriture des données
Les scripts d’écritures se trouvent dans 2 fichiers (un coorespondant à chaque système de stockage) : 
-	insertion_matar_impact_geo.py
-	insertion_matar_impact_temporelle.py


## Travail effectué et règle d'éxécution

*Les 2 systèmes de stockage ont été créé dans 2 différentes sessions, on préférera donc séparer les éxécutions selon les sessions*

### objectif 1

Ainsi toutes les fonctions permettant de répondre à l’objectif 1 se trouve dans *execution_impact_geo.py*.
Les fonctions d’exécutions sont :
-	*execute_plot_geo*
-   *execute_plot_mean_saisonnier_samy()*

### Objectifs 2 et 3

Toutes les fonctions permettant de répondre aux objectifs 2 et 3, ainsi que la fonction permettant de créer se trouve dans execution_impact_temporelle.py.
Les fonctions d’exécutions sont :

-	*execute_plot_mean_temperature()*
-	*execute_clusterisation_pour_afficher_points_sur_carte()*
A noter que cette dernière fonction va nous crée un fichier CSV avec la localisation des centres des différents clusters,
ainsi que les températures associés. Pour pouvoir afficher les résultats sur une carte, 
il faut exécuter le contenu du fichier *exectute_carte_italie.py* avec le fichier CSV renvoyé 
par la fonction *execute_clusterisation_pour_afficher_points_sur_carte()*



## Lecture des données
Les fonctions permettant de lire les données se trouvent dans les fichiers de travail respectifs cités ci-dessus.
Elles commencent toute dans leur nom par *‘**generateur**_xxx’*

