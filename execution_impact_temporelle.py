﻿#travaillez avec spark
#source /pyspark.env

from scipy.spatial import Voronoi, voronoi_plot_2d
from cassandra.cluster import Cluster
from pyspark import SparkContext
from datetime import datetime
from datetime import timedelta
from math import *

import matplotlib.pyplot as plt
import numpy as np
import calendar
import copy 
import csv
import re

cluster = Cluster(['localhost'])
session = cluster.connect('sbenslim_project_nf26')
#session = cluster.connect('projet_liujijie')

	
#FONCTION DE LECTURES ----------------------------------------------------------------------------------

#function qui retourne les lignes pour un jour donnée
def generateurAllResult():
	query = """ SELECT date_year, date_month, date_day , date_hour, date_min , latitude, longitude,
				tmpf FROM matar_impact_temporelle ALLOW FILTERING;"""
	res = session.execute(query)
	
	for r in res:
		yield r

#function qui retourne les lignes pour un jour donnée
def generateurResult(jourVoulu):
	t = (jourVoulu['year'], jourVoulu['month'], jourVoulu['day'])
	query = """ SELECT * FROM matar_impact_temporelle WHERE date_year=%s and date_month=%s and date_day=%s;"""
	res = session.execute(query, t)
	
	for r in res:
		yield r
	
#function qui retorune les lignes pour une periode (d'un jour à un autre) donnée	
def generateurResultPeriod(jour1, jour2):
	d1 = datetime(jour1['year'], jour1['month'], jour1['day'])
	d2 = datetime(jour2['year'], jour2['month'], jour2['day'])

	while (d1<=d2):
		t = (d1.year, d1.month, d1.day)
		query = """ SELECT * FROM matar_impact_temporelle WHERE date_year=%s and date_month=%s and date_day=%s;"""
		out = session.execute(query, t)
		for r in out:
			yield r
		d1+=timedelta(days=1)		

		
#function qui retourne les lignes pour un jour donnée
def generateurResultAccurate(jourVoulu):
	t = (jourVoulu['year'], jourVoulu['month'], jourVoulu['day'], jourVoulu['hour'], jourVoulu['minute'])
	query = """ SELECT * FROM matar_impact_temporelle WHERE date_year=%s and date_month=%s and date_day=%s and date_hour=%s and date_min=%s;"""
	res = session.execute(query, t)
	
	for r in res:
		yield r
		
	
	

#OBJECTIF 2 -------------------------------------------------------------------------------------------

#Return a list of a list of each day of month for a year given 
def generatorToutesLesDates(anneeVoulu):
	res = []
	for i in range(1,13):
		nbsDaysMonth = calendar.monthrange(anneeVoulu,i)
		listMonth = []
		for j in range(1,nbsDaysMonth[1]+1):
			dj={}
			dj['year'] = anneeVoulu
			dj['month'] = i
			dj['day'] = j
			listMonth.append(dj)
		res.append(listMonth)
	return res
	
#function compute temperature mean for each month of a given year
def mean_tempeature_month_for_a_year(anneeVoulu):
	year = generatorToutesLesDates(anneeVoulu)
	mean = {}
	i=1
	for month in year:
		somme=0
		tot=0
		for day in month:
			dataResult = generateurResult(day)
			for dr in dataResult:
				#Moyenne
				somme += dr.tmpf
				tot+=1
		if (tot==0):
			mean[str(i)] = 150 #valeur abérantes
		else:
			mean[str(i)] = somme/tot
		i+=1
	#print("moyenne par mois pour l'année ",anneeVoulu, ": ", mean , " T")
	return mean
	
#function compute temperature mean for each day of a given month
def mean_tempeature_day_for_a_month(anneeVoulu,moisVoulu):
	year = generatorToutesLesDates(anneeVoulu)
	month = year[moisVoulu-1]
	mean = {}
	for day in month:
		somme=0
		tot=0
		dataResult = generateurResult(day)
		for dr in dataResult:
			#Moyenne
			somme += dr.tmpf
			tot+=1
		if (tot==0):
			mean[str(day['day'])] = 150
		else:
			mean[str(day['day'])] = somme/tot
	#print("moyenne par jour pour le mois ", moisVoulu ," de l''année ",anneeVoulu, ": ", mean, " T")
	return mean

	
#plot la courbe des moyennes selon les mois d'une année donnée
def plot_mean_tempeature_month_for_a_year(anneeVoulu):
	mean = mean_tempeature_month_for_a_year(anneeVoulu)
	lists = mean.items() # sorted by key, return a list of tuples
	x, y = zip(*lists) # unpack a list of pairs into two tuples
	fig = plt.figure()
	plt.plot(x,y, "o-")
	plt.title(str(anneeVoulu)+": Moyenne des températures en Italie par Mois")
	plt.xlabel("Mois")
	plt.ylabel("Température (en °T)")
	fig.savefig('plot/plot_mean_temperature_'+str(anneeVoulu)+'.png')
	return 0

#plot la courbe des moyennes selon les jours d'un mois et d'une année données
def plot_mean_tempeature_day_for_a_month(anneeVoulu,moisVoulu):
	mean = mean_tempeature_day_for_a_month(anneeVoulu,moisVoulu)
	lists = mean.items() # sorted by key, return a list of tuples
	x, y = zip(*lists) # unpack a list of pairs into two tuples
	fig = plt.figure()
	plt.plot(x,y, "o-")
	plt.title(str(moisVoulu) + " " + str(anneeVoulu)+": Moyenne des températures en Italie par jour")
	plt.xlabel("Jour du mois")
	plt.ylabel("Température (en °T)")
	fig.savefig('plot/plot_mean_temperature_'+str(moisVoulu)+"_"+str(anneeVoulu)+'.png')
	return 0

	
#affichage des températures sur les centres des classes K sur la carte d'Italie à instant donnée 
#return fichier csv avec les centres des classes et la température moyenne en Fahreneit de chaque cluster
#EXECUTER ENSUITE LE FICHIER RESULTAT CREE EN LOCAL avec le code dans le fichier exectute_carte_italie.py 
def affichage_carte(dateVoulu, k):
	
	#recuperer les données
	out = generateurResult(dateVoulu)
	
	#centres des k classes
	centres = k_means_distribue(dateVoulu,k)
	
	#calculez mean temperature pour chaque classe
	info_class = []
	for i in range(k):
		info_class.append([0,0])
	
	for d in out:
		position = {'lon': d.longitude, 'lat': d.latitude}
		w = appartenance_classe(position, centres)
		info_class[w][0]+=1
		info_class[w][1]+=d.tmpf
	
	temp_class = []
	temp_class.append(['lon', 'lat', 'temperature'])
	for pos,i in zip(centres,info_class):
		if i[0]==0:
			mean_t=0
		else:
			mean_t=i[1]/i[0]
		temp_class.append([pos['lon'], pos['lat'],round(mean_t, 1)])
	
	with open('map_info.csv', 'w') as csvfile:
		spamwriter = csv.writer(csvfile, delimiter=';',
								quotechar='|', quoting=csv.QUOTE_MINIMAL)
		spamwriter.writerows(temp_class)
	
	
#OBJECTIF 3 : CLUSTERISATION -------------------------------------------------------------------------

#return la classe d'appartenance du point en se basant sur la plus petite des distances entre ce point et les centres des classes
def appartenance_classe(point, centres):
	classe=0
	distance = []
	#calcul distance entre chaque point et chaque centre
	for c in centres:
		dist = sqrt((point['lon']-c['lon'])**2 + (point['lat']-c['lat'])**2)
		distance.append(dist)
	return distance.index(min(distance))
	
#une étape de clusterisation par la méthode des k-means
def clusterisation(centres, dataset, k): 
	#etape 0 : init des futurs centres et du nbs d'elements classés dans chaque cluster
	accu_centroide = []
	acccu_count = []
	for i in range(k):
		accu_centroide.append({'lon': 0,'lat': 0})
		acccu_count.append(0)
	
	#etape 1 : sommer les points appartenant à chaque classe
	for data in dataset:
		position = {'lon': data.longitude, 'lat': data.latitude}
		w = appartenance_classe(position, centres)

		#on additionne les points appartenant à la classe
		accu_centroide[w]['lon']+=data.longitude
		accu_centroide[w]['lat']+=data.latitude
		
		#nbs de points appartenant à chaque classe
		acccu_count[w]+= 1

	#etape 2 : créer les nouveaux centres des classes
	for c,nbs in zip(accu_centroide,acccu_count):
		if(nbs==0):
			c['lon'] = 0
			c['lat'] = 0
		else:
			c['lon'] = c['lon']/nbs
			c['lat'] = c['lat']/nbs
	
	#etape 3 : retourne les nouveaux centres
	return accu_centroide

#return True si les tous les points des centres sont égaux	
def equal_centres(centres1, centres2):
	for c1,c2 in zip(centres1, centres2):
		if (c1['lon']!=c2['lon'] or c1['lat']!=c2['lat']):
			return False
	return True
	
#entrée : les dates de la periode voulue pour clusteriser nos données (à noter que les dates sont le jour,mois et année) et le nombre de cluster voulu
#fonction qui pour une periode donnée, retourne les centres (lon,lat) des k clusters 
def k_means_distribue(date1, k):

	#init des centres
	out = generateurResult(date1)
	centres = []
	for i in range(k):
		d = out.__next__()
		cinit = {}
		cinit['lon']=d.longitude
		cinit['lat']=d.latitude
		centres.append(cinit)
	print("init centres :", centres)
	
	#RECUPERER LES DONNEES
	out = generateurResult(date1)
	new_centres = clusterisation(centres, out, k)
	
	#on reitere l'update des centres jusqu'a ce que les centres se stabilisent ou nbs_iteration>10000
	nbs_iteration=0    
	while (equal_centres(centres,new_centres)!=True and nbs_iteration<10000):
		out = generateurResult(date1)
		centres = copy.deepcopy(new_centres)
		new_centres = clusterisation(centres,out, k)
		
		nbs_iteration+=1
	
	#arroundir les mocalisations
	for c in new_centres:
		c['lon']= round(c['lon'], 4)
		c['lat']= round(c['lat'], 4)
		
	print("")
	print("Centres finaux :", new_centres)
	print("")
	print("nombre d'iterations : ", nbs_iteration)
	return new_centres
	
#TODO : fonction qui affiche les points en fonction des clusters auxquels ils appartient (diagramme de Voronoï)
def affichage_clusterisation(date1, k):
    #on recupere les centres des clusters
    centres = k_means_distribue(date1,k)
    list_cen = []
    for c in centres:
        list_cen.append([c['lon'],c['lat']])
    
    fig = plt.figure()
    d1= str(date1['day']) + "-" + str(date1['month']) + "-" + str(date1['year'])
    #d2= str(date2['day']) + "-" + str(date2['month']) + "-" + str(date2['year'])

    #plot voronoi
    #vor = Voronoi(list_cen)
    #voronoi_plot_2d(vor)
    
    #on recupere et plot les points
    out = generateurResult(date1)
    for data in out:
        pos = {'lon': data.longitude,'lat': data.latitude}
        classe = appartenance_classe(pos, centres)
        
        #plot points 
        plt.scatter(np.array([data.longitude]),np.array([data.latitude]), c=[classe], s=5, cmap='summer') 
    
    #plot les centres
    tab_cen = np.array(list_cen)
    plt.scatter(tab_cen[:, 0], tab_cen[:, 1], c='black', s=100, alpha=0.5)

    plt.title("Clusterisation des localisations du " + d1)
    fig.savefig('plot/varanoi_'+d1+'.png')		
		

		
#TEST EXECUTION --------------------------------------------------------------------------------------
	
#OBJECTIF 2 : 
#def execute_affichage_carte():	

#Sortie : Créer une image et l'enregistre dans le dossier 'plot'
def execute_plot_mean_temperature():
	print("Voulez-vous plot les moyennes des températures selon : ")
	print("    - les mois d'une année (1)")
	print("    -des jours d'un mois précis (2)")
	choix = input()
	
	if int(choix)==1:
		print("choisissez l'année au format 'yyyy' [2011-2018] : ")
		y = input()
		plot_mean_tempeature_month_for_a_year(int(y))
	else:
		print("choisissez le mois au format 'm' [1-12] : ")
		m = input()
		print("choisissez l'année au format 'yyyy' [2011-2018] : ")
		y = input()
		plot_mean_tempeature_day_for_a_month(int(y), int(m))
	return 0

		

#OBJECTIF 3 :

#Sortie : Créer un fichier CSV et l'enregistre dans le dossier racine sous le nom de 'map_info.csv'
def execute_clusterisation_pour_afficher_points_sur_carte():
	print("Instant souhaité -------------------")
	print("---- exemple de date pour un test que vous pouvez prendre : 01/01/2011 -----")
	print("Veuillez entrer le jour au format 'd' [1-31] : ")
	d1 = input()
	print("Veuillez entrer le mois 'm' [1-12]: ")
	m1 = input()
	print("Veuillez entrer l'année 'yyyy' [2011-2018] : ")
	y1 = input()	
	#print("Veuillez entrer l'heure : ")
	#h1 = input()	
	#print("Veuillez entrer la minute : ")
	#min1 = input()
	
	#print("jour 2 -------------------")
	#print("Veuillez entrer le jour : ")
	#d2 = input()
	#print("Veuillez entrer le mois : ")
	#m2 = input()
	#print("Veuillez entrer l'année : ")
	#y2 = input()
	
	print("")
	print("Veuillez entrez la clusterisation voulue :")
	print("   - clusterisation par secteur (k=3) : tapez 0")
	print("   - clusterisation par region [FORTEMENT CONSEILLE] (k=20) : tapez 1")
	
	ch = input()
	if (int(ch)==0):
		k=3
	else:
		k=20
	
	j1 = {}
	j1['year']=int(y1)
	j1['month']=int(m1)
	j1['day']=int(d1)
	#j1['hour']=int(h1)
	#j1['minute']=int(min1)
	
	#j2 = {}
	#j2['year']=int(y2)
	#j2['month']=int(m2)
	#j2['day']=int(d2)
	print(j1,"--",k)
	res = affichage_carte(j1,k)
	return 0
	
