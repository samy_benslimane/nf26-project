from cassandra.cluster import Cluster
import csv
import re
import numpy as np

cluster = Cluster(['localhost'])
session = cluster.connect('projet_liujijie')

#GENERATEURS ET LIMITEUR ---------------------------------------------------------------------------------

def loadData(filename):
	dateparser = re.compile(
		"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+)"
	)
	with open(filename) as f:
		for r in csv.DictReader(f):
			match_start = dateparser.match(r["valid"])
			if not match_start:
				continue
			start = match_start.groupdict()
			data = {}
			
			data["timestamp"] = (
				int(start["year"]),
				int(start["month"]),
				int(start["day"]),
				int(start["hour"]),
				int(start["minute"]),
			)
			
			data["lon"] = float(r["lon"])
			data["lat"] = float(r["lat"])
			data["station"] = r["station"]
			
			#colonnes
			if r["tmpf"]=='null':
				data["tmpf"]=None
			else:
				data["tmpf"] = float(r["tmpf"])
				
			if r["dwpf"]=='null':
				data["dwpf"]=None
			else:
				data["dwpf"] = float(r["dwpf"])
				
			if r["relh"]=='null':
				data["relh"]=None
			else:
				data["relh"] = float(r["relh"])
				
			if r["drct"]=='null':
				data["drct"]=None
			else:
				data["drct"] = float(r["drct"])
				
			if r["sknt"]=='null':
				data["sknt"]=None
			else:
				data["sknt"] = float(r["sknt"])
				
			if r["alti"]=='null':
				data["alti"]=None
			else:
				data["alti"] = float(r["alti"])
			#data["p01i"] = int(r["p01i"])
            #data["mslp"] = int(r["mslp"])

			if r["vsby"]=='null':
				data["vsby"]=None
			else:
				data["vsby"] = float(r["vsby"])
            #data["gust"] = int(r["gust"])
			if r["skyc1"]=='null':
				data["skyc1"]=None
			else:
				data["skyc1"] = r["skyc1"]
				
			if r["skyc2"]=='null':
				data["skyc2"]=None
			else:
				data["skyc2"] = r["skyc2"]
				
			if r["skyc3"]=='null':
				data["skyc3"]=None
			else:
				data["skyc3"] = r["skyc3"]
				
			if r["skyc4"]=='null':
				data["skyc4"]=None
			else:
				data["skyc4"] = r["skyc4"]
				
			if r["skyl1"]=='null':
				data["skyl1"]=None
			else:
				data["skyl1"] = float(r["skyl1"])
				
			if r["skyl2"]=='null':
				data["skyl2"]=None
			else:
				data["skyl2"] = float(r["skyl2"])
				
			if r["skyl3"]=='null':
				data["skyl3"]=None
			else:
				data["skyl3"] = float(r["skyl3"])
				
			if r["skyl4"]=='null':
				data["skyl4"]=None
			else:
				data["skyl4"] = float(r["skyl4"])
			
			#data["wxcodes"] = int(r["wxcodes"])
			#data["ice_accretion_1hr"] = int(r["ice_accretion_1hr"])
			#data["ice_accretion_3hr"] = int(r["ice_accretion_3hr"])
			#data["ice_accretion_6hr"] = int(r["ice_accretion_6hr"])
			#data["peak_wind_gust"] = int(r["peak_wind_gust"])
			#data["peak_wind_drct"] = int(r["peak_wind_drct"])
			#data["peak_wind_time"] = int(r["peak_wind_time"])
			#data["feel"] = float(r["feel"])
			
			if r["metar"]=='null':
				data["metar"]=None
			else:
				data["metar"] = r["metar"]
				
			yield data
		
def limiteur(g, limit):
	for i,d in enumerate(g):
		if i>limit:
			return None
		yield d
	

#function to write in Cassandra
def writecassandra(csvfilename):
	ds = loadData(csvfilename)
	# ds = limiteur(ds,1000)
	for r in ds:
		t = (
				r["timestamp"][0],
				r["timestamp"][1],
				r["timestamp"][2],
				
				r["timestamp"][3],
				r["timestamp"][4],
				r["station"],
				r["lat"],
				r["lon"],
				
				r["tmpf"],
				r["dwpf"],
				r["relh"],
				r["drct"],
				r["sknt"],
				r["alti"],
				r["vsby"],
				r["skyc1"],
				r["skyc2"],
				r["skyc3"],
				r["skyc4"],
				r["skyl1"],
				r["skyl2"],
				r["skyl3"],
				r["skyl4"],
				r["metar"],				
			)

		query = """
		INSERT INTO matar_impact_geo(
			date_year,
			date_month,
			date_day,
			date_hour,
			date_min,
			station,
			latitude,
			longitude,
			tmpf,
			dwpf,
			relh,
			drct,
			sknt,
			alti,
			vsby,
			skyc1,
			skyc2,
			skyc3,
			skyc4,
			skyl1,
			skyl2,
			skyl3,
			skyl4,
			metar
			)
		VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
		"""
		session.execute(query, t)

# EXECUTE
# writecassandra('asos_db.csv')