#travaillez avec spark
#source /pyspark.env

#lancez spark
#pyspark

from pyspark import SparkContext
from cassandra.cluster import Cluster
from math import *

import matplotlib.pyplot as plt
import numpy as np

cluster = Cluster(['localhost'])
session = cluster.connect('projet_liujijie')

def limgen(gen, limit):
	for i,d in enumerate(gen):
		if i>=limit:
			return None
		yield d

#  PRIMARY KEY ((latitude, longitude), date_year, date_month, date_day, date_hour, date_min)

def generateurResult_onePoint(latVoulu, lonVoule):
	t = (latVoulu, lonVoule)
	query = """ SELECT latitude, longitude, date_year, date_month, tmpf FROM matar_impact_geo WHERE latitude=%s and longitude=%s;"""
	res = session.execute(query, t)
	
	for r in res:
		yield r

#function qui retourne les lignes pour une locatisation donnée (longitude et latitude)
def generateurResultLocation(lat, lon):
	t = (lat,lon)
	query = """ SELECT * FROM matar_impact_geo WHERE latitude=%s and longitude=%s;"""
	res = session.execute(query, t)
	
	for r in res:
		yield r

# test
# lat = 42.73
# lon = 10.38
# D = sc.parallelize(generateurResult_onePoint(lat,  lon))

#OBJECTIF 1 -------------------------------------------------------------------------------------------

def mared(A,B):
	a0,a1 = A
	b0,b1 = B
	return (a0+b0,a1+b1)
	
#transformer la clé en (saison,année)
#1 -> 9/10/11  ||  2 ->12/1/2  ||  3 -> 3/4/5   || 4 -> 6/7/8  
def mapSaison(k):
	if(k[0]==9 or k[0]==10 or k[0]==11):
		kN = (1,k[1])
	elif(k[0]==12 or k[0]==1 or k[0]==2):
		kN = (2,k[1])
	elif(k[0]==3 or k[0]==4 or k[0]==5):
		kN = (3,k[1])
	elif(k[0]==6 or k[0]==7 or k[0]==8):
		kN = (4,k[1])
	return kN

	
#affichage des saisonnalité température mean par saison par de janvier 2011 à decembre 2018 pour une localisaton donnée
def calcul_mean(lat,lon):
	sc = SparkContext(appName="Find Max Int")
	sc.setLogLevel("ERROR")
	data = generateurResultLocation(lat,lon)
	D = sc.parallelize(data)
	rdd1 = D.map(lambda d: ((d.date_month, d.date_year) , (1,d.tmpf)))
	rdd1b = rdd1.filter(lambda d :(d[1][1] is not None))
	rdd2 = rdd1b.reduceByKey(lambda a,b : mared(a,b))
	rdd3 = rdd2.map(lambda d: (mapSaison(d[0]),d[1]))
	rdd4 = rdd3.reduceByKey(lambda a,b : mared(a,b))
	rdd5 = rdd4.map(lambda d: (d[0], d[1][1]/d[1][0])) 
	rdd6 = rdd5.sortBy(lambda d: d[0][1]) 
	return rdd6.collect()

	
#plot_mean_saisonnier(42.73,10.38) : permets de créer le plot voulu
def plot_mean_saisonnier(lat, lon):
	saison = ['1','2','3','4']
	name_saison = ["Automne","Hiver","Printemps","Ete"]
	couleurs = ['red', 'green', 'blue', 'purple']
	i=0
	
	#récupérer nos données saisonnière annuel
	res = calcul_mean(lat, lon)
	
	fig = plt.figure()
	for sais, col in zip(saison,couleurs):
		annee = []
		temperature = []
		for r in res:
			if(str(r[0][0])==sais):
				lab=name_saison[i]
				annee.append(str(r[0][1]))
				temperature.append(r[1])
				
		plt.plot(annee,temperature, 'o-', c=col, label=lab)
		i+=1
		
	plt.legend(loc=4)	
	plt.title("Moyenne des températures en Italie pour chaque saison période 2011-2018")
	plt.xlabel("Année")
	plt.ylabel("Température (en °T)")
	fig.savefig('plot/plot_temperature_entre_2011_et_2018.png')
	

# pour chaque année, tmpf moyenne saison
def getSeason(month):
	season = 0
	if month >= 3 and month <=5:
		season = 1
	elif month >= 6 and month <=8:
		season = 2
	elif month >= 9 and month <=11:
		season = 3
	else:
		season = 4
	return season

def mean_saison_chaqueAnnee_function(D):
	rdd1 = D.map(lambda d: ((d.date_year, getSeason(d.date_month)), np.array([1, d.tmpf])))
	rdd2 = rdd1.filter(lambda d :(d[1][1] is not None))
	rdd3 = rdd2.reduceByKey(lambda a,b: a+b)
	rdd4 = rdd3.map(lambda m:(m[0], (m[1][1]/m[1][0])))
	return rdd4.collect()

def mean_saison_chaqueMois_function(D):
	rdd1 = D.map(lambda d: ((d.date_year, d.date_month, getSeason(d.date_month)), np.array([1, d.tmpf])))
	rdd2 = rdd1.filter(lambda d :(d[1][1] is not None))
	rdd3 = rdd2.reduceByKey(lambda a,b: a+b)
	rdd4 = rdd3.map(lambda m:(m[0], (m[1][1]/m[1][0])))
	return rdd4.collect()

# test
# mean_saison_chaqueAnnee = mean_saison_chaqueAnnee_function(D)
# mean_saison_chaqueMois = mean_saison_chaqueMois_function(D)


# plot
import matplotlib.pyplot as plt
import pandas as pd
def plot_mean_temperature_saison(mean_saison_chaqueAnnee, lat, lon):
	fig = plt.figure()
	year = [d[0][0] for d in mean_saison_chaqueAnnee]
	time = [d[0][1] for d in mean_saison_chaqueAnnee]
	y = [d[1] for d in mean_saison_chaqueAnnee]
	df = pd.concat([pd.DataFrame(year), pd.DataFrame(time), pd.DataFrame(y)], axis=1)
	df.columns = ['year', 'x', 'y']
	df = df.sort_values(by=['x'])
	for annee in np.sort(df.year.unique()):
		x = ["{}-{}".format(annee, d) for d in df.loc[df['year'] == annee,]['x']]
		y = df.loc[df['year'] == annee,]['y']
		plt.plot(x, y, "o-", label = str(annee))
	plt.xlabel("temps")
	plt.ylabel("Température (en °T)")
	plt.title(str(lat)+', '+str(lon)+": Moyenne des températures d'une point")
	plt.legend(loc='best')
	fig.set_size_inches(12.5, 8.5)
	fig.savefig('plot/plot_mean_temperature_saison_('+str(lat)+', '+str(lon)+').png')

# test
# plot_mean_temperature_saison(mean_saison_chaqueMois, lat, lon)

def plot_boxplot_saision(mean_saison_chaqueMois, lat, lon):
	fig = plt.figure()
	time = [d[0][2] for d in mean_saison_chaqueMois]
	y = [d[1] for d in mean_saison_chaqueMois]
	df = pd.concat([pd.DataFrame(time), pd.DataFrame(y)], axis=1)
	df.columns = ['x', 'y']
	df = df.sort_values(by=['x'])
	data = [df.loc[df['x'] == 1,]['y'], df.loc[df['x'] == 2,]['y'], df.loc[df['x'] == 3,]['y'], df.loc[df['x'] == 4,]['y']]
	label = ['printemps', 'ete', 'autonme', 'hiver']
	plt.boxplot(data, labels=label, sym ="o", whis = 1.5)
	plt.xlabel("temps")
	plt.ylabel("Température (en °T)")
	plt.title(str(lat)+', '+str(lon)+": les températures d'une point")
	fig.savefig('plot/boxplot_saison_('+str(lat)+', '+str(lon)+').png')

# test
# plot_boxplot_saision(mean_saison_chaqueMois, lat, lon)

#TEST EXECUTION --------------------------------------------------------------------------------------
# env: pyspark
# autre ville: lat = 40.5582, lon = 14.2023

#Sortie : Créer une image et l'enregistre dans le dossier 'plot'
def execute_plot_mean_saisonnier_samy():
	print("Nous allons exécutez ce script pour la ville de Livourne (Italie) : ")
	print("latitude : 42.73")
	print("longitude : 10.38")
	
	#execution
	plot_mean_saisonnier(42.73,10.38)
	return 0

def execute_plot_geo():
	sc = SparkContext.getOrCreate()

	print("Voulez-vous plot les moyennes des températures d'une place, ")
	print("Exemple: on travaile sur la ville de Livourne en Italie, latitude = 42.73, longitude = 10.38")
	print("    - lancer l'exemple (1)")
	print("    - obtenir le plot d'une autre ville (2)")
	choix = input()

	if int(choix)==1:
		lat = 42.73
		lon = 10.38
		print("------Exemple------")
		D = sc.parallelize(generateurResult_onePoint(lat,  lon))
		print("    - les courbes (1)")
		print("    - les boxplots (2)")
		plotForm = input()
		if int(plotForm) == 1:
			mean_saison_chaqueAnnee = mean_saison_chaqueAnnee_function(D)
			plot_mean_temperature_saison(mean_saison_chaqueAnnee, lat, lon)
		else: 
			mean_saison_chaqueMois = mean_saison_chaqueMois_function(D)
			plot_boxplot_saision(mean_saison_chaqueMois, lat, lon)
	else:
		print("latitude: ")
		a = input()
		print("longitude: ")
		b = input()
		lat = float(a)
		lon = float(b)
		D = sc.parallelize(generateurResult_onePoint(lat,  lon))
		print("    - les courbes (1)")
		print("    - les boxplots (2)")
		plotForm = input()
		if int(plotForm) == 1:
			mean_saison_chaqueAnnee = mean_saison_chaqueAnnee_function(D)
			plot_mean_temperature_saison(mean_saison_chaqueAnnee, lat, lon)
		else: 
			mean_saison_chaqueMois = mean_saison_chaqueMois_function(D)
			plot_boxplot_saision(mean_saison_chaqueMois, lat, lon)
		


