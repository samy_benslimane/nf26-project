#A EXECUTER APRES EXECUTER execute_clusterisation_pour_afficher_points_sur_carte(), en important le fichier CSV dans le même répértoire que ce fichier

#map des centres pour le jour mois année 01/01/2011
import csv
from jyquickhelper import add_notebook_menu
add_notebook_menu()

import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(7,7))
ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
ax.set_extent((5.053, 22.72, 36.15, 46.53))

ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.COASTLINE)
ax.add_feature(cfeature.BORDERS, linestyle=':')

dial = csv.excel
dial.delimiter=';'
with open('map_info.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile, dialect=dial)
    out = [dict(row) for row in reader]
        
for t in out:
    lon = float(t['lon'])
    lat = float(t['lat'])
    tempe = float(t['temperature'])

    ax.plot(lon,lat, '.')
    ax.annotate(tempe, (lon,lat))
ax.set_title('ITALY');